package net.codeandcoder.BasicTicTacToe.players;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Position;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;
import net.codeandcoder.BasicTicTacToe.io.InputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;

public class HumanPlayer extends Player {

    private InputManager input = null;
    private OutputManager output = null;
    
    public HumanPlayer(String name, SquareState playerSymbol, InputManager input) {
        super(name, playerSymbol);
        this.input = input;
    }
    
    public HumanPlayer(String name, SquareState playerSymbol, InputManager input,
            OutputManager output) {
        super(name, playerSymbol);
        this.input = input;
        this.output = output;
    }

    @Override
    public Position playTurn(Board b) {
        Position validPosition = null;
        
        while (validPosition == null) {
            if (output != null) {
                output.write("Choose X: ");
            }
            Integer x = Integer.parseInt(input.read());
            if (output != null) {
                output.write("Choose Y: ");
            }
            Integer y = Integer.parseInt(input.read());
            
            if (x < 0 || y < 0 || x >= b.getSize() || y >= b.getSize() ||
                    b.getSquare(x,y).getValue() != SquareState.Void) {
                output.write("The introduced position is not valid. Try another one.");
            } else {
                validPosition = new Position(x, y);
            }
        }
        
        return validPosition;
    }

}
