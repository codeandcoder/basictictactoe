package net.codeandcoder.BasicTicTacToe.players;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Position;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;

public class SmartAIPlayer extends Player {
    
    public static final int MINIMAX_WIN_VALUE = 10;
    public static final int MINIMAX_DEPTH_VALUE = 1;
    
    public SmartAIPlayer(String name, SquareState playerSymbol) {
        super(name, playerSymbol);
    }

    @Override
    public Position playTurn(Board b) {
        List<MinimaxResult> minMaxList = minimax(b, true, 0);
        return minMaxList.get(0).pos;
    }
    
    /**
     * Minimax algorithm implementation
     * 
     * @param b
     * @param playerMove
     * @param depth
     * @return
     */
    private List<MinimaxResult> minimax(Board b, boolean playerMove, int depth) {
    	// Choose the symbol to use in the next simulation step
        SquareState symbol = playerMove ? playerSymbol : playerSymbol == SquareState.X ? SquareState.O : SquareState.X;
        List<Position> voidPositions = b.getVoidPositions();
        List<MinimaxResult> minimaxOptions = new ArrayList<>();
        // Try all possible permutations
        for (int i = 0; i < voidPositions.size(); i++) {
            Board newBoard = b.copyBoard();
            // Simulate the movement in a temporary board
            newBoard.setSquare(voidPositions.get(i), symbol);
            SquareState winnerSymbol = newBoard.checkWinner();
            // If there is no winner yet, simulate the next movement
            if (winnerSymbol == SquareState.Void) {
                List<MinimaxResult> resultList = minimax(newBoard, !playerMove, depth+1);
                // If the result of the next movement simulation is empty,
                // it means that the board is full and it is a draw
                if (resultList.isEmpty()) {
                    MinimaxResult result = new MinimaxResult();
                    result.pos = voidPositions.get(i);
                    result.value = 0;
                    minimaxOptions.add(result);
                } else if (!playerMove) {
                	// If the player did the last simulation, get the Max value
                	// from the list of results (Max part of miniMAX)
                    MinimaxResult result = resultList.get(0);
                    result.pos = voidPositions.get(i);
                    minimaxOptions.add(result);
                } else {
                	// If the opponent did the last simulation, get the Min value
                	// from the list of results (Min part of MINimax)
                    MinimaxResult result = resultList.get(resultList.size()-1);
                    result.pos = voidPositions.get(i);
                    minimaxOptions.add(result);
                }
            } else {
            	// If we have a winner, and the winner is the player, count that simulation step as postive
                if (winnerSymbol == playerSymbol) {
                    MinimaxResult minimax = new MinimaxResult();
                    minimax.pos = voidPositions.get(i);
                    // We take a penalty from depth (better to win early than later)
                    minimax.value = MINIMAX_WIN_VALUE  - depth * MINIMAX_DEPTH_VALUE;
                    minimaxOptions.add(minimax);
                } else {
                	// Otherwise, count that simulation step as negative
                    MinimaxResult minimax = new MinimaxResult();
                    minimax.pos = voidPositions.get(i);
                    minimax.value = -MINIMAX_WIN_VALUE  - depth * MINIMAX_DEPTH_VALUE;
                    minimaxOptions.add(minimax);
                }
            }
        }
        
        Collections.sort(minimaxOptions);
        return minimaxOptions;
    }
    
    class MinimaxResult implements Comparable<MinimaxResult> {
        
        public int value;
        public Position pos;

        @Override
        public int compareTo(MinimaxResult arg0) {
            return arg0.value - value;
        }
        
    }

}
