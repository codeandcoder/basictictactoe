package net.codeandcoder.BasicTicTacToe.players;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Position;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;
import net.codeandcoder.BasicTicTacToe.io.InputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;

public class LearningAIPlayer extends Player {

    private Random random;
    private Map<String, List<PositionLearning>> learning;
    private TreeMap<String, Position> gameHistory = new TreeMap<>();
    
    public LearningAIPlayer(String name, SquareState playerSymbol, InputManager input) {
        super(name, playerSymbol);
        random = new Random();
        learning = readLearning(input);
    }

    @Override
    public Position playTurn(Board b) {
        Position choosenPosition = null;
        String boardKey = b.toString();
        if (learning.containsKey(boardKey)) {
            List<PositionLearning> positions = getBestOptions(learning.get(boardKey));
            int rdmNumber = Math.abs(random.nextInt()) % positions.size();
            choosenPosition = positions.get(rdmNumber).position;
        } else {
            List<PositionLearning> positionslearning = new ArrayList<>();
            List<Position> voidPositions = b.getVoidPositions();
            for (int i = 0; i < voidPositions.size(); i++) {
                Position pos = voidPositions.get(i);
                PositionLearning posLearning = new PositionLearning(pos, 0);
                positionslearning.add(posLearning);
            }
            learning.put(boardKey, positionslearning);
            int rdmNumber = Math.abs(random.nextInt()) % voidPositions.size();
            choosenPosition = voidPositions.get(rdmNumber);
        }
        
        gameHistory.put(boardKey, choosenPosition);
        
        return choosenPosition;
    }
    
    private Map<String, List<PositionLearning>> readLearning(InputManager input) {
        Map<String, List<PositionLearning>> learningMap = new HashMap<>();
        // Read learning from a file
        if (input != null) {
            String line = input.read();
            while (line != null && !"".equals(line.trim())) {
                String[] parts = line.split("%");
                String key = parts[0];
                String[] posLearnings = parts[1].split("#");
                List<PositionLearning> formattedPosLearnings = new ArrayList<>();
                for (String pl : posLearnings) {
                    String[] plParts = pl.split(":");
                    String[] posXY = plParts[0].split(",");
                    Position pos = new Position(Integer.parseInt(posXY[0]), Integer.parseInt(posXY[1]));
                    PositionLearning formattedPL = new PositionLearning(pos, Integer.parseInt(plParts[1]));
                    formattedPosLearnings.add(formattedPL);
                }
                learningMap.put(key, formattedPosLearnings);
                line = input.read();
            }
            input.close();
        }
        return learningMap;
    }
    
    public void saveLearning(int learningValue, OutputManager output) {
        for (int i = 0; i < gameHistory.size(); i++) {
            Map.Entry<String, Position> e = gameHistory.pollLastEntry();
            setLearningValue(learning.get(e.getKey()), e.getValue(), learningValue - i - 1);
        }
        if (output != null) {
            // Write learning to a file
            for(Map.Entry<String, List<PositionLearning>> e : learning.entrySet()) {
                String line = e.getKey() + "%";
                for (int i = 0; i < e.getValue().size(); i++) {
                    PositionLearning pl = e.getValue().get(i);
                    line += pl.toString() + "#";
                }
                output.write(line);
            }
        }
    }
    
    private void setLearningValue(List<PositionLearning> list, Position pos, int value) {
        for (int i = 0; i < list.size(); i++) {
            PositionLearning pl = list.get(i);
            if (pl.position.equals(pos)) {
                pl.value += value;
                pl.value = pl.value > 1000 ? 1000 : pl.value < -1000 ? -1000 : pl.value;
                return;
            }
        }
    }
    
    private List<PositionLearning> getBestOptions (List<PositionLearning> list) {
        List<PositionLearning> result = new ArrayList<>();
        Collections.sort(list);
        int bestValue = list.get(0).value;
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).value < bestValue) {
                break;
            } else {
                result.add(list.get(i));
            }
        }
        return result;
    }
    
    class PositionLearning implements Comparable<PositionLearning> {
        
        public Position position;
        public int value;
        
        public PositionLearning(Position pos, int value) {
            this.position = pos;
            this.value = value;
        }
        
        @Override
        public String toString() {
            return position.toString() + ":" + value;
        }

        @Override
        public int compareTo(PositionLearning o) {
            return o.value - value;
        }
        
    }

}
