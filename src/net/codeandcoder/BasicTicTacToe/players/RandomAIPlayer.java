package net.codeandcoder.BasicTicTacToe.players;

import java.util.List;
import java.util.Random;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Position;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;

public class RandomAIPlayer extends Player {

    private Random random;
    
    public RandomAIPlayer(String name, SquareState playerSymbol) {
        super(name, playerSymbol);
        random = new Random();
    }

    @Override
    public Position playTurn(Board b) {
        List<Position> voidPositions = b.getVoidPositions();
        int rdmNumber = Math.abs(random.nextInt()) % voidPositions.size();
        return voidPositions.get(rdmNumber);
    }

}
