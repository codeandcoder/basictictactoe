package net.codeandcoder.BasicTicTacToe.players;

import java.util.Random;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Position;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;

public class AdaptativeAIPlayer extends Player {

	private RandomAIPlayer randomMind;
	private SmartAIPlayer smartMind;
    
	private Random random;
    private float randomChance = 50;
    
    public AdaptativeAIPlayer(String name, SquareState playerSymbol) {
        super(name, playerSymbol);
        random = new Random();
        randomMind = new RandomAIPlayer(name, playerSymbol);
        smartMind = new SmartAIPlayer(name, playerSymbol);
    }

    @Override
    public Position playTurn(Board b) {
    	Position selectedPosition = null;
    	int randomNumber = Math.abs(random.nextInt() % 100);
    	if (randomNumber < randomChance) {
    		selectedPosition = randomMind.playTurn(b);
    	} else {
    		selectedPosition = smartMind.playTurn(b);
    	}
        return selectedPosition;
    }
    
    public void updateRandomChance(float value) {
    	randomChance += value;
    	randomChance = randomChance > 100 ? 100 : randomChance < 0 ? 0 : randomChance;
    }
    
}
