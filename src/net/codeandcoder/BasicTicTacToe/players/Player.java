package net.codeandcoder.BasicTicTacToe.players;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Position;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;

public abstract class Player {

    protected String name = "";
    protected SquareState playerSymbol = SquareState.Void;
    
    public Player(String name, SquareState playerSymbol) {
        this.name = name;
        this.playerSymbol = playerSymbol;
    }
    
    public abstract Position playTurn(Board b);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SquareState getPlayerSymbol() {
        return playerSymbol;
    }

    public void setPlayerSymbol(SquareState playerSymbol) {
        this.playerSymbol = playerSymbol;
    }
    
}
