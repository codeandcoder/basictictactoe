package net.codeandcoder.BasicTicTacToe.game;

import java.util.ArrayList;
import java.util.List;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Position;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;
import net.codeandcoder.BasicTicTacToe.listeners.TurnListener;
import net.codeandcoder.BasicTicTacToe.players.Player;

public class TicTacToe {
    
    private static List<TurnListener> turnListeners = new ArrayList<>();
    private static int turnNumber = 0;

    public static Player play(Board board, List<Player> players) {
        Player winner = null;
        
        int squaresLeft = board.getVoidPositions().size();
        turnNumber = board.getSize() * board.getSize() - squaresLeft;
        
        for (int i = 0; i < players.size() && winner == null && squaresLeft-i != 0; i++) {
            Player player = players.get(i);
            Position move = player.playTurn(board);
            board.setSquare(move, player.getPlayerSymbol());
            callListeners(turnNumber, board, player, move);
            
            SquareState winnerSymbol = board.checkWinner();
            if (winnerSymbol != SquareState.Void) {
                winner = findPlayerWithSymbol(players, winnerSymbol);
            }
        }
        
        return winner;
    }
    
    private static void callListeners(int turn, Board board, Player player, Position move) {
        for (TurnListener tl : turnListeners) {
            tl.onTurnEnded(turn, board, player, move);
        }
    }
    
    private static Player findPlayerWithSymbol(List<Player> players, SquareState symbol) {
        for (Player p : players) {
            if (p.getPlayerSymbol() == symbol) {
                return p;
            }
        }
        
        return null;
    }
    
    public static void registerListener(TurnListener tl) {
        turnListeners.add(tl);
    }
    
    public static int getLastTurnNum() {
        return turnNumber;
    }
}
