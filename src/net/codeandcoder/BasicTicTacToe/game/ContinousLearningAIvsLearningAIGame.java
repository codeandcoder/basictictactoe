package net.codeandcoder.BasicTicTacToe.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;
import net.codeandcoder.BasicTicTacToe.io.FileInputManager;
import net.codeandcoder.BasicTicTacToe.io.FileManager;
import net.codeandcoder.BasicTicTacToe.io.FileOutputManager;
import net.codeandcoder.BasicTicTacToe.io.InputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;
import net.codeandcoder.BasicTicTacToe.players.LearningAIPlayer;
import net.codeandcoder.BasicTicTacToe.players.Player;

public class ContinousLearningAIvsLearningAIGame extends Game {

    public ContinousLearningAIvsLearningAIGame(OutputManager output, InputManager input) {
        super(output, input);
    }

    @Override
    public Result play() {
        Result result = new Result();
        
        int player1Score = 0;
        int player2Score = 0;
        
        String bobLearningFile = ".ttt_bob_learning";
        String bobyLearningFile = ".ttt_boby_learning";
        FileInputManager bobInputFile = new FileInputManager();
        bobInputFile.setFile(FileManager.getStoringPath(), bobLearningFile);
        FileInputManager bobyInputFile = new FileInputManager();
        bobyInputFile.setFile(FileManager.getStoringPath(), bobyLearningFile);
        LearningAIPlayer player1 = new LearningAIPlayer("Bob", SquareState.X, bobInputFile);
        LearningAIPlayer player2 = new LearningAIPlayer("Boby", SquareState.O, bobyInputFile);
        
        List<Player> players = new ArrayList<>();
        players.add(player1);
        players.add(player2);
        
        for (int i = 0; i < 10000; i++) {
            // Ensuring randomness and X moves first
            Collections.shuffle(players);
            players.get(0).setPlayerSymbol(SquareState.X);
            players.get(1).setPlayerSymbol(SquareState.O);
            
            // Create a standard Tic Tac Toe board 3x3
            Board board = new Board(3);
            
            // Game loop
            Player winner = null;
            while(board.getVoidPositions().size() > 0 && winner == null) {
                winner = TicTacToe.play(board, players);
            }
            
            if (winner != null && winner.equals(player1)) {
                player1Score++;
                player1.saveLearning(TicTacToe.getLastTurnNum(), null);
                player2.saveLearning(-TicTacToe.getLastTurnNum(), null);
            } else if (winner != null && winner.equals(player2)) {
                player2Score++;
                player1.saveLearning(-TicTacToe.getLastTurnNum(), null);
                player2.saveLearning(TicTacToe.getLastTurnNum(), null);
            }
        }
        
        FileOutputManager bobOutManager = new FileOutputManager();
        bobOutManager.setFile(FileManager.getStoringPath(), bobLearningFile);
        player1.saveLearning(0, bobOutManager);
        bobOutManager.close();
        
        FileOutputManager bobyOutManager = new FileOutputManager();
        bobyOutManager.setFile(FileManager.getStoringPath(), bobyLearningFile);
        player2.saveLearning(0, bobyOutManager);
        bobyOutManager.close();
        
        output.write("Bob won " + player1Score + " times");
        output.write("Boby won " + player2Score + " times");
        
        if (player1Score > player2Score) {
            result.setWinner(player1);
        } else if (player2Score > player1Score) {
            result.setWinner(player2);
        } else {
            result.setWinner(null);
        }
        
        return result;
    }

}
