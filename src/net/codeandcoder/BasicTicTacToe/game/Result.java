package net.codeandcoder.BasicTicTacToe.game;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.players.Player;

public class Result {

    private Board finalBoard;
    private Player winner;
    
    public Board getFinalBoard() {
        return finalBoard;
    }
    public void setFinalBoard(Board finalBoard) {
        this.finalBoard = finalBoard;
    }
    public Player getWinner() {
        return winner;
    }
    public void setWinner(Player winner) {
        this.winner = winner;
    }
    
}
