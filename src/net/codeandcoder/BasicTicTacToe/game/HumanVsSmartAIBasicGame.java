package net.codeandcoder.BasicTicTacToe.game;

import java.util.ArrayList;
import java.util.List;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;
import net.codeandcoder.BasicTicTacToe.io.InputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;
import net.codeandcoder.BasicTicTacToe.players.HumanPlayer;
import net.codeandcoder.BasicTicTacToe.players.Player;
import net.codeandcoder.BasicTicTacToe.players.RandomAIPlayer;
import net.codeandcoder.BasicTicTacToe.players.SmartAIPlayer;

public class HumanVsSmartAIBasicGame extends Game {

    public HumanVsSmartAIBasicGame(OutputManager output, InputManager input) {
        super(output, input);
    }

    @Override
    public Result play() {
        Result result = new Result();
        
        // Create a standard Tic Tac Toe board 3x3
        Board board = new Board(3);
        
        // Instantiate the two players with random X/O assignment
        SquareState humanSymbol = Math.round(Math.random()) == 0 
                ? SquareState.X : SquareState.O;
        List<Player> players = new ArrayList<>();
        // Ensure that the X is always first (X moves first)
        if (humanSymbol == SquareState.X) {
            players.add(new HumanPlayer("You", humanSymbol, input, output));
            players.add(new SmartAIPlayer("Percy", SquareState.O));
        } else {
            players.add(new SmartAIPlayer("Percy", SquareState.X));
            players.add(new HumanPlayer("You", humanSymbol, input, output));
        }
        
        // Game loop
        Player winner = null;
        while(board.getVoidPositions().size() > 0 && winner == null) {
            winner = TicTacToe.play(board, players);
        }
        
        result.setFinalBoard(board);
        result.setWinner(winner);
        
        return result;
    }

}
