package net.codeandcoder.BasicTicTacToe.game;

import java.util.ArrayList;
import java.util.List;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;
import net.codeandcoder.BasicTicTacToe.io.InputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;
import net.codeandcoder.BasicTicTacToe.players.Player;
import net.codeandcoder.BasicTicTacToe.players.RandomAIPlayer;
import net.codeandcoder.BasicTicTacToe.players.SmartAIPlayer;

public class SmartAIvsDumbAIBasicGame extends Game {

    public SmartAIvsDumbAIBasicGame(OutputManager output, InputManager input) {
        super(output, input);
    }

    @Override
    public Result play() {
        Result result = new Result();
        
        // Create a standard Tic Tac Toe board 3x3
        Board board = new Board(3);
        
        // Instantiate the two players with random X/O assignment
        SquareState bobSymbol = Math.round(Math.random()) == 0 
                ? SquareState.X : SquareState.O;
        List<Player> players = new ArrayList<>();
        // Ensure that the X is always first (X moves first)
        if (bobSymbol == SquareState.X) {
            players.add(new SmartAIPlayer("Bob", bobSymbol));
            players.add(new RandomAIPlayer("Percy", SquareState.O));
        } else {
            players.add(new RandomAIPlayer("Percy", SquareState.X));
            players.add(new SmartAIPlayer("Bob", bobSymbol));
        }
        
        // Game loop
        Player winner = null;
        while(!board.getVoidPositions().isEmpty() && winner == null) {
            winner = TicTacToe.play(board, players);
        }
        
        result.setFinalBoard(board);
        result.setWinner(winner);
        
        return result;
    }

}
