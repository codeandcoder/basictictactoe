package net.codeandcoder.BasicTicTacToe.game;

import net.codeandcoder.BasicTicTacToe.io.InputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;


public abstract class Game {

    protected OutputManager output;
    protected InputManager input;
    
    public Game(OutputManager output, InputManager input) {
        this.output = output;
        this.input = input;
    }

    public abstract Result play();
    
}
