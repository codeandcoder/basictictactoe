package net.codeandcoder.BasicTicTacToe.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;
import net.codeandcoder.BasicTicTacToe.io.FileInputManager;
import net.codeandcoder.BasicTicTacToe.io.FileManager;
import net.codeandcoder.BasicTicTacToe.io.FileOutputManager;
import net.codeandcoder.BasicTicTacToe.io.InputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;
import net.codeandcoder.BasicTicTacToe.players.LearningAIPlayer;
import net.codeandcoder.BasicTicTacToe.players.Player;
import net.codeandcoder.BasicTicTacToe.players.RandomAIPlayer;

public class ContinousDumbAIvsLearningAIGame extends Game {

    public ContinousDumbAIvsLearningAIGame(OutputManager output, InputManager input) {
        super(output, input);
    }

    @Override
    public Result play() {
        Result result = new Result();
        
        int dumbScore = 0;
        int learningScore = 0;
        
        String bobLearningFile = ".ttt_bob_learning";
        FileInputManager fileInputManager = new FileInputManager();
        fileInputManager.setFile(FileManager.getStoringPath(), bobLearningFile);
        RandomAIPlayer dumbPlayer = new RandomAIPlayer("Percy", SquareState.X);
        LearningAIPlayer learningPlayer = new LearningAIPlayer("Bob", SquareState.O, fileInputManager);
        
        List<Player> players = new ArrayList<>();
        players.add(dumbPlayer);
        players.add(learningPlayer);
        
        for (int i = 0; i < 10000; i++) {
            // Ensuring randomness and X moves first
            Collections.shuffle(players);
            players.get(0).setPlayerSymbol(SquareState.X);
            players.get(1).setPlayerSymbol(SquareState.O);
            
            // Create a standard Tic Tac Toe board 3x3
            Board board = new Board(3);
            
            // Game loop
            Player winner = null;
            while(board.getVoidPositions().size() > 0 && winner == null) {
                winner = TicTacToe.play(board, players);
            }
            
            if (winner != null && winner.equals(dumbPlayer)) {
                dumbScore++;
                learningPlayer.saveLearning(-TicTacToe.getLastTurnNum(), null);
            } else if (winner != null && winner.equals(learningPlayer)) {
                learningScore++;
                learningPlayer.saveLearning(TicTacToe.getLastTurnNum(), null);
            }
        }
        
        FileOutputManager fileOutManager = new FileOutputManager();
        fileOutManager.setFile(FileManager.getStoringPath(), bobLearningFile);
        learningPlayer.saveLearning(0, fileOutManager);
        fileOutManager.close();
        
        output.write("Bob won " + learningScore + " times");
        output.write("Percy won " + dumbScore + " times");
        
        if (dumbScore > learningScore) {
            result.setWinner(dumbPlayer);
        } else if (learningScore > dumbScore) {
            result.setWinner(learningPlayer);
        } else {
            result.setWinner(null);
        }
        
        return result;
    }

}
