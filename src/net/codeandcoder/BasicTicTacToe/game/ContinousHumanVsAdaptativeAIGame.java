package net.codeandcoder.BasicTicTacToe.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;
import net.codeandcoder.BasicTicTacToe.io.InputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;
import net.codeandcoder.BasicTicTacToe.players.AdaptativeAIPlayer;
import net.codeandcoder.BasicTicTacToe.players.HumanPlayer;
import net.codeandcoder.BasicTicTacToe.players.Player;
import net.codeandcoder.BasicTicTacToe.players.SmartAIPlayer;

public class ContinousHumanVsAdaptativeAIGame extends Game {

    public ContinousHumanVsAdaptativeAIGame(OutputManager output, InputManager input) {
        super(output, input);
    }

    @Override
    public Result play() {
    	Result result = new Result();
    	boolean exit = false;
    	
    	int humanScore = 0;
    	int aiScore = 0;
    	
    	HumanPlayer humanPlayer = new HumanPlayer("You", SquareState.X, input, output);
    	AdaptativeAIPlayer adaptativePlayer = new AdaptativeAIPlayer("Bob", SquareState.O);
    	
    	List<Player> players = new ArrayList<>();
        players.add(humanPlayer);
        players.add(adaptativePlayer);
    	
    	while (!exit) {
    		// Ensuring randomness and X moves first
    		Collections.shuffle(players);
    		players.get(0).setPlayerSymbol(SquareState.X);
    		players.get(1).setPlayerSymbol(SquareState.O);
    		
	        // Create a standard Tic Tac Toe board 3x3
	        Board board = new Board(3);
	        
	        // Game loop
	        Player winner = null;
	        while(board.getVoidPositions().size() > 0 && winner == null) {
	            winner = TicTacToe.play(board, players);
	        }
	        
	        if (winner != null && winner.equals(humanPlayer)) {
	        	humanScore++;
	        	adaptativePlayer.updateRandomChance(-20);
	        } else if (winner != null && winner.equals(adaptativePlayer)) {
	        	aiScore++;
	        	adaptativePlayer.updateRandomChance(20);
	        } else {
	        	adaptativePlayer.updateRandomChance(5);
	        }
	        
	        output.write("Do you want to exit now? Y/N");
	        exit = input.read().equalsIgnoreCase("Y");
    	}
    	
    	if (humanScore > aiScore) {
    		result.setWinner(humanPlayer);
    	} else if (aiScore < humanScore) {
    		result.setWinner(adaptativePlayer);
    	} else {
    		result.setWinner(null);
    	}
        
        return result;
    }
}
