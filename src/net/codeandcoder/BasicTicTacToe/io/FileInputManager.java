package net.codeandcoder.BasicTicTacToe.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileInputManager implements InputManager {

    private BufferedReader br;
    
    public boolean setFile(String path, String fileName) {
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        
        File file = new File(path + File.separator + fileName);
        
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            br = new BufferedReader(new FileReader(file));
        } catch (IOException ex) {
            return false;
        }
        
        
        return true;
    }
    
    @Override
    public String read() {
        try {
            return br.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        return null;
    }
    
    @Override
    public void close() {
        try {
            br.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
