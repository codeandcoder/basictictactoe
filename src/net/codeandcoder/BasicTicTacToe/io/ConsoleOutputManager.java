package net.codeandcoder.BasicTicTacToe.io;

public class ConsoleOutputManager implements OutputManager {

    @Override
    public void write(String line) {
        System.out.println(line);
    }
    
    @Override
    public void close() {}
    
}
