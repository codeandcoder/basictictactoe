package net.codeandcoder.BasicTicTacToe.io;

public interface InputManager {

    public abstract String read();
    public abstract void close();
    
}
