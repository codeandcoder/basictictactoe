package net.codeandcoder.BasicTicTacToe.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileOutputManager implements OutputManager {

    private String content = "";
    private File file;
    
    public boolean setFile(String path, String fileName) {
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        
        file = new File(path + File.separator + fileName);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException ex) {
            return false;
        }
        
        return true;
    }
    
    @Override
    public void write(String line) {
        content += line + "\n";
    }
    
    @Override
    public void close() {
        try {
            FileWriter fw = new FileWriter(file);
            fw.write(content);
            fw.flush();
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
