package net.codeandcoder.BasicTicTacToe.io;

public interface OutputManager {

    public abstract void write(String line);
    public abstract void close();
    
}
