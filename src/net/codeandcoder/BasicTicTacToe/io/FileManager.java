package net.codeandcoder.BasicTicTacToe.io;

public class FileManager {

    public static String getStoringPath() {
        String path = "";
        String os = System.getProperty("os.name").toLowerCase();
        if ( os.contains("win") ) {
            String username = System.getProperty("user.name");
            path = "C:/Users/" + username + "/AppData/Roaming";
        } else {
            path = System.getProperty("user.home");
        }
        return path;
    }

}
