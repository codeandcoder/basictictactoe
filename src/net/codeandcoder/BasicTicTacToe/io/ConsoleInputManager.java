package net.codeandcoder.BasicTicTacToe.io;

import java.util.Scanner;

public class ConsoleInputManager implements InputManager {

    private Scanner sc = new Scanner(System.in);
    
    @Override
    public String read() {
        return sc.next();
    }

    @Override
    public void close() {}
    
}
