package net.codeandcoder.BasicTicTacToe;

import net.codeandcoder.BasicTicTacToe.game.AIvsAIBasicGame;
import net.codeandcoder.BasicTicTacToe.game.ContinousDumbAIvsLearningAIGame;
import net.codeandcoder.BasicTicTacToe.game.ContinousHumanVsAdaptativeAIGame;
import net.codeandcoder.BasicTicTacToe.game.ContinousLearningAIvsLearningAIGame;
import net.codeandcoder.BasicTicTacToe.game.Game;
import net.codeandcoder.BasicTicTacToe.game.HumanVsSmartAIBasicGame;
import net.codeandcoder.BasicTicTacToe.game.Result;
import net.codeandcoder.BasicTicTacToe.game.SmartAIvsDumbAIBasicGame;
import net.codeandcoder.BasicTicTacToe.game.TicTacToe;
import net.codeandcoder.BasicTicTacToe.io.ConsoleInputManager;
import net.codeandcoder.BasicTicTacToe.io.ConsoleOutputManager;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;
import net.codeandcoder.BasicTicTacToe.listeners.OutputTurnListener;

public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // Select type of game
        Game game = new ContinousDumbAIvsLearningAIGame(new ConsoleOutputManager(), new ConsoleInputManager());
        // Select output
        OutputManager output = new ConsoleOutputManager();
        // Register listener
        TicTacToe.registerListener(new OutputTurnListener(output));
        // Play
        Result result = game.play();
        
        // Output result
        if (result.getWinner() != null) {
            output.write(result.getWinner().getName() + " won!!!");
        } else {
            output.write("It is a draw...");
        }
        output.close();
    }

}
