package net.codeandcoder.BasicTicTacToe.board;

public final class Square {

    public enum SquareState { X('X'), O('O'), Void(' ');
        public char symbol;
        private SquareState(char symbol) {
            this.symbol = symbol;
        }
    };
    private SquareState value = SquareState.Void;
    
    public Square() {}
    
    public Square(SquareState value) {
        this.value = value;
    }
    
    public SquareState getValue() {
        return value;
    }
    public void setValue(SquareState value) {
        this.value = value;
    }
    
}
