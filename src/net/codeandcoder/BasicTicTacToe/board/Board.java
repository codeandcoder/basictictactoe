package net.codeandcoder.BasicTicTacToe.board;

import java.util.ArrayList;
import java.util.List;

import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;

public final class Board {

    private Square[][] board;
    
    private Position lastMove;
    
    // Constructors
    public Board(int size) {
        this.board = new Square[size][size];
        initSquares();
    }
    
    public Board(Square[][] board) {
        this.board = board.clone();
    }
    
    public List<Position> getVoidPositions() {
        List<Position> positions = new ArrayList<>();
        
        for (int y = 0; y < getSize(); y++) {
            for (int x = 0; x < getSize(); x++) {
                Square s = getSquare(x,y);
                if (s.getValue() == SquareState.Void) {
                    positions.add(new Position(x,y));
                }
            }
        }
        
        return positions;
    }
    
    public SquareState checkWinner() {
        SquareState winner = SquareState.Void;
        
        if (lastMove != null) {
            winner = checkWinner(lastMove);
        }
        
        return winner;
    }
    
    private SquareState checkWinner(Position lastMove) {
        SquareState winner = SquareState.Void;
        
        // Check horizontal
        boolean rowComplete = true;
        Square previousSquare = getSquare(0, lastMove.getY());
        for (int x = 1; x < getSize() && rowComplete; x++) {
            Square currentSquare = getSquare(x, lastMove.getY());
            rowComplete = currentSquare.getValue() == previousSquare.getValue();
            previousSquare.setValue(currentSquare.getValue());
        }
        
        if (!rowComplete) {
            // CheckVertical
            rowComplete = true;
            previousSquare = getSquare(lastMove.getX(), 0);
            for (int y = 1; y < getSize() && rowComplete; y++) {
                Square currentSquare = getSquare(lastMove.getX(), y);
                rowComplete = currentSquare.getValue() == previousSquare.getValue();
                previousSquare.setValue(currentSquare.getValue());
            }
            
            if (!rowComplete && lastMove.getX() == lastMove.getY()) {
                // Check Diagonal 1
                rowComplete = true;
                previousSquare = getSquare(0, 0);
                for (int xy = 1; xy < getSize() && rowComplete; xy++) {
                    Square currentSquare = getSquare(xy, xy);
                    rowComplete = currentSquare.getValue() == previousSquare.getValue();
                    previousSquare.setValue(currentSquare.getValue());
                }
            }
                
            if (!rowComplete 
                    && getSize() - 1 - lastMove.getX() == lastMove.getY()) {
                
                // Check Diagonal 2
                rowComplete = true;
                previousSquare = getSquare(0, getSize()-1);
                for (int x = 1; x < getSize() && rowComplete; x++) {
                    int y = getSize() - 1 - x;
                    Square currentSquare = getSquare(x, y);
                    rowComplete = currentSquare.getValue() == previousSquare.getValue();
                    previousSquare.setValue(currentSquare.getValue());
                }
            }
        }
        
        if (rowComplete) {
            winner = getSquare(lastMove).getValue();
        }
        
        return winner;
    }
    
    private void initSquares() {
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board[0].length; x++) {
                board[x][y] = new Square(SquareState.Void);
            }
        }
    }
    
    @Override
    public String toString() {
        String result = "";
        for (int x = 0; x < getSize(); x++) {
            for (int y = 0; y < getSize(); y++) {
                result += getSquare(x, y).getValue().symbol;
            }
        }
        return result;
    };
    
    // Getters && Setters
    public Square getSquare(int x, int y) {
        return new Square(board[y][x].getValue());
    }
    
    public Square getSquare(Position pos) {
        return new Square(board[pos.getY()][pos.getX()].getValue());
    }
    
    public void setSquare(int x, int y, SquareState value) {
        board[y][x].setValue(value);
        lastMove = new Position(x, y);
    }
    
    public void setSquare(Position pos, SquareState value) {
        board[pos.getY()][pos.getX()].setValue(value);
        lastMove = new Position(pos.getX(), pos.getY());
    }
    
    public int getSize() {
        return board.length;
    }
    
    public Board copyBoard() {
        Board clone = new Board(getSize());
        for (int x = 0; x < getSize(); x++) {
            for (int y = 0; y < getSize(); y++) {
                clone.setSquare(x, y, getSquare(x, y).getValue());
            }
        }
        return clone;
    }
    
}
