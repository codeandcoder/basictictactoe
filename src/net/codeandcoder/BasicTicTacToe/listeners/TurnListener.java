package net.codeandcoder.BasicTicTacToe.listeners;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Position;
import net.codeandcoder.BasicTicTacToe.players.Player;

public interface TurnListener {

    public abstract void onTurnEnded(int turn, Board board, Player player, Position move);
    
}
