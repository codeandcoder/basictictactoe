package net.codeandcoder.BasicTicTacToe.listeners;

import net.codeandcoder.BasicTicTacToe.board.Board;
import net.codeandcoder.BasicTicTacToe.board.Position;
import net.codeandcoder.BasicTicTacToe.board.Square.SquareState;
import net.codeandcoder.BasicTicTacToe.io.OutputManager;
import net.codeandcoder.BasicTicTacToe.players.Player;

public class OutputTurnListener implements TurnListener {

    private OutputManager output;
    
    
    public OutputTurnListener(OutputManager output) {
        this.output = output;
    }
    
    @Override
    public void onTurnEnded(int turn, Board board, Player player, Position move) {
        output.write("Turn: " + turn);
        output.write(player.getName() + " choose X:" + move.getX() + " Y:" + move.getY());
        printBoard(board);
    }
    
    private void printBoard(Board board) {
        /*
         *    0 1 2
         *   -------
         * 0 |X|O| |
         * 1 | |X|O|
         * 2 |X| |O|
         *   -------
         */
        
        // header
        String headerLine1 = "    ";
        String headerLine2 = "   ";
        for (int i = 0; i < board.getSize(); i++) {
            headerLine1 += i + " ";
        }
        output.write(headerLine1);
        
        for (int i = 0; i < board.getSize(); i++) {
            headerLine2 += "--";
        }
        headerLine2 += "-";
        output.write(headerLine2);
        
        // body
        for (int y = 0; y < board.getSize(); y++) {
            String line = " " + y + " |";
            for (int x = 0; x < board.getSize(); x++) {
                char pos = board.getSquare(x,y).getValue().symbol;
                line += pos + "|";
            }
            output.write(line);
        }
        // bottom
        output.write(headerLine2);
    }

}
